/*

   - Mohammad Ateyyah Salah
       - 161077
           - HW_2

*/
import 'package:flutter/material.dart';

import 'widgets/category_card.dart';
import 'widgets/into_cart.dart';
import 'widgets/items_section.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Homework 2',
      home: MyHomePage(title: 'Homework-2'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    double screenWidth =
        MediaQuery.of(context).size.width; // needed for responsive design
    double screenHeight =
        MediaQuery.of(context).size.height; // needed for responsive design

    return Scaffold(
      body: Padding(
        // padding to the screen
        padding: EdgeInsets.only(
            top: screenHeight * 0.03,
            right: screenWidth * 0.025,
            left: screenWidth * 0.025),
        child: Column(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: screenHeight * 0.025),
                child: Text(
                  "Center",
                  style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            InfoCard(), // The big blue card
            ItemsSection(), // The four small icons
            Expanded(
              // The last 4 cards
              child: ListView(padding: EdgeInsets.zero, children: [
                CategoryCard('Address', 'Ensure your harvesting address',
                    Icons.location_on, Colors.deepPurple[300]),
                CategoryCard('Privacy', 'System permission change', Icons.lock,
                    Colors.pink[300]),
                CategoryCard('General', 'Basic functional settings',
                    Icons.layers, Colors.orange[300]),
                CategoryCard('Notification', 'Take over the news in time',
                    Icons.notifications, Colors.cyan[300]),
              ]),
            ),
          ],
        ),
      ),
    );
  }
}
