import 'package:flutter/material.dart';

class CategoryCard extends StatelessWidget {
  final title;
  final description;
  final icon;
  final iconColor;
  const CategoryCard(
    this.title,
    this.description,
    this.icon,
    this.iconColor, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Container(
      margin: EdgeInsets.symmetric(vertical: screenHeight * 0.0125),
      decoration:
          BoxDecoration(borderRadius: BorderRadius.circular(20), boxShadow: [
        BoxShadow(blurRadius: 5, color: Colors.deepPurple[50], spreadRadius: 3)
      ]),
      height: screenHeight * 0.095,
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        child: Padding(
          padding: EdgeInsets.all(screenWidth * 0.025),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CircleAvatar(
                radius: screenHeight * 0.035,
                backgroundColor: iconColor,
                child: Icon(
                  icon,
                  color: Colors.white,
                  size: screenHeight * 0.035,
                ),
              ),
              SizedBox(
                width: screenWidth * 0.025,
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      description,
                      style: TextStyle(fontSize: 13, color: Colors.black26),
                    )
                  ],
                ),
              ),
              Icon(
                Icons.chevron_right,
                color: Colors.black26,
              )
            ],
          ),
        ),
      ),
    );
  }
}
