import 'package:flutter/material.dart';

import 'text_column.dart';

class InfoCard extends StatelessWidget {
  const InfoCard({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Card(
      clipBehavior: Clip.antiAlias,
      elevation: 5,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      child: Container(
        height: screenHeight * 0.22,
        color: Colors.blue,
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(screenWidth * 0.05),
              child: Column(
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        radius: screenHeight * 0.04,
                        backgroundImage: AssetImage('assets/images/F.png'),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: screenWidth * 0.05),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Text(
                                  'Mausam Rayamajhi',
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  width: screenWidth * 0.04,
                                ),
                                Icon(
                                  Icons.edit,
                                  color: Colors.white,
                                ),
                              ],
                            ),
                            Text(
                              'A trendsetter',
                              style: TextStyle(color: Colors.white70),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                TextColumn('846', 'Collect'),
                TextColumn('51', 'Attention'),
                TextColumn('267', 'Track'),
                TextColumn('39', 'Coupons'),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
