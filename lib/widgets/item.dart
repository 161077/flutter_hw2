import 'package:flutter/material.dart';

class Item extends StatelessWidget {
  final itemIcon;
  final itemText;
  const Item(
    this.itemIcon,
    this.itemText, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Column(
      children: [
        CircleAvatar(
          radius: screenWidth * 0.065,
          backgroundColor: Colors.grey[100],
          child: Padding(
            padding: EdgeInsets.all(screenWidth * 0.0375),
            child: Image(image: AssetImage(itemIcon)),
          ),
        ),
        SizedBox(
          height: screenHeight * 0.014,
        ),
        Text(
          itemText,
          style: TextStyle(fontSize: 15),
        )
      ],
    );
  }
}
