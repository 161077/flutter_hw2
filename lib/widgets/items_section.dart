import 'package:flutter/material.dart';

import 'item.dart';

class ItemsSection extends StatelessWidget {
  const ItemsSection({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Padding(
      padding: EdgeInsets.symmetric(vertical: screenHeight * 0.03),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Item('assets/images/wal.png', 'Wallet'),
          Item('assets/images/del.png', 'Delivery'),
          Stack(children: [
            Item('assets/images/mes.png', 'Message'),
            Container(
              width: screenWidth * 0.15,
              height: screenHeight * 0.07,
              child: Align(
                alignment: Alignment(1, -0.9),
                child: CircleAvatar(
                  backgroundColor: Colors.blue,
                  radius: screenWidth * 0.02,
                  child: Text(
                    '2',
                    style: TextStyle(color: Colors.white, fontSize: 13),
                  ),
                ),
              ),
            )
          ]),
          Item('assets/images/ser.png', 'Service'),
        ],
      ),
    );
  }
}
