import 'package:flutter/material.dart';

class TextColumn extends StatelessWidget {
  final String upperText;
  final String lowerText;
  const TextColumn(
    this.upperText,
    this.lowerText, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;

    return Column(
      children: [
        Text(
          upperText,
          style: TextStyle(
              color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: screenHeight * 0.0075,
        ),
        Text(lowerText, style: TextStyle(color: Colors.white70, fontSize: 15)),
      ],
    );
  }
}
